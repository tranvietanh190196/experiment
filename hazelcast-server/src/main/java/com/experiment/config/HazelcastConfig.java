package com.experiment.config;


import com.experiment.serialization.ISerializer;
import com.hazelcast.config.Config;
import com.hazelcast.config.FileSystemYamlConfig;
import com.hazelcast.config.SerializerConfig;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.config.JetConfig;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Configuration
//@EnableConfigurationProperties
public class HazelcastConfig {

    @SneakyThrows
    @Bean("jetEmbedded")
    public JetInstance jetEmbedded(@Value("${hazelcast.config}") String hazelcastConfig, List<ISerializer> serializers){
        List<SerializerConfig> serializerConfigs = serializers.stream()
                .map(ISerializer::toSerializerConfig)
                .collect(Collectors.toList());

        JetConfig jetConfig = new JetConfig();
        jetConfig.getHazelcastConfig().getSerializationConfig().setSerializerConfigs(serializerConfigs);

        Config cfg = new FileSystemYamlConfig(hazelcastConfig);
        jetConfig.setHazelcastConfig(cfg);
        return Jet.newJetInstance(jetConfig);
    }

}
