package com.experiment.config;


import com.experiment.serialization.ISerializer;
import com.hazelcast.config.Config;
import com.hazelcast.config.FileSystemXmlConfig;
import com.hazelcast.config.SerializerConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.config.JetClientConfig;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Configuration
@EnableConfigurationProperties
public class HazelcastConfig {

    //    @Bean
    @SneakyThrows
    public HazelcastInstance hazelcastInstance(@Value("${hazelcast.config}") String hazelcastConfig) {
        Config cfg =  new FileSystemXmlConfig(hazelcastConfig);
        return Hazelcast.newHazelcastInstance(cfg);
    }

    @Lazy
    @Bean("jetClusterClient")
    public JetInstance jetClusterClient(@Value("${hazelcast.cluster}") String[] clusterInfo, List<ISerializer> serializers){
        List<SerializerConfig> serializerConfigs = serializers.stream().map(ISerializer::toSerializerConfig).collect(Collectors.toList());
        log.info("SerializerConfigs: {}", serializerConfigs);
        JetClientConfig config = new JetClientConfig();
        config.getNetworkConfig().addAddress(clusterInfo);
        config.getSerializationConfig().setSerializerConfigs(serializerConfigs);

        return Jet.newJetClient(config);
    }

}
