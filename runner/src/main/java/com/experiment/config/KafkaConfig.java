package com.experiment.config;

import com.experiment.util.KafkaBuilder;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.producer.Producer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaConfig {
    @Value("${kafka.broker}")
    private String brokers;
    @Value("${kafka.topic}")
    private String topic;

    @Bean
    public Producer<Long, String> kafkaProducer(){
        return KafkaBuilder.builder()
                .brokers(brokers)
                .build().getProducer();
    }

    @Bean
    public Consumer<Long, String> kafkaConsumer(){
        return KafkaBuilder.builder()
                .brokers(brokers)
                .autocommit(false)
                .maxPollRecords(100)
                .offsetReset(true)
                .messageCount(1000)
                .maxNoMessageFoundCount(100)
                .topic(topic)
                .groupId("experimentGroup")
                .build().getConsumer();
    }
}
