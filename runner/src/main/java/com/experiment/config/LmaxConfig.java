package com.experiment.config;

import com.experiment.lmax.*;
import com.experiment.util.Utils;
import com.lmax.disruptor.BatchEventProcessor;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.SequenceBarrier;
import com.lmax.disruptor.YieldingWaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.util.DaemonThreadFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class LmaxConfig {

    private static final int RING_SIZE = 8;
    private final ExecutorService executor = Executors.newFixedThreadPool(3, DaemonThreadFactory.INSTANCE);


    @Bean("ring_buffer")
    public RingBuffer<Event> getRingBuffer() {
        Disruptor<Event> disruptor = Utils.createSingleProducer(new BaseEventFactory(), new YieldingWaitStrategy(), RING_SIZE);
        disruptor.handleEventsWith(new FirstConsumer());
        return disruptor.start();
    }

    /*
        Multiple Consumer
     */
    @Bean("ring_buffer_2")
    public RingBuffer<Event> getRingBuffer2() {
        Disruptor<Event> disruptor = Utils.createSingleProducer(new BaseEventFactory(), new YieldingWaitStrategy(), RING_SIZE);

        RingBuffer<Event> ringBuffer = disruptor.getRingBuffer();
        final SequenceBarrier sequenceBarrier = ringBuffer.newBarrier();

        final FirstConsumer firstConsumer = new FirstConsumer();
        final SecondConsumer secondConsumer = new SecondConsumer();
        final ThirdConsumer thirdConsumer = new ThirdConsumer();

        final BatchEventProcessor<Event> firstBatch = new BatchEventProcessor<>(ringBuffer, sequenceBarrier, firstConsumer);
        final BatchEventProcessor<Event> secondBatch = new BatchEventProcessor<>(ringBuffer, sequenceBarrier, secondConsumer);

        final SequenceBarrier sequenceBarrierConclusion = ringBuffer.newBarrier(firstBatch.getSequence(), secondBatch.getSequence());
        final BatchEventProcessor<Event> thirdBatch = new BatchEventProcessor<>(ringBuffer, sequenceBarrierConclusion, thirdConsumer);

        ringBuffer.addGatingSequences(thirdBatch.getSequence());

        executor.submit(firstBatch);
        executor.submit(secondBatch);
        executor.submit(thirdBatch);

        return ringBuffer;
    }

}
