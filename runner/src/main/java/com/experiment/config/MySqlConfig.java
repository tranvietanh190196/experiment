package com.experiment.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@Log4j2
@Configuration
@EnableTransactionManagement
@ConfigurationProperties(prefix = "mysql.datasource")
public class MySqlConfig extends HikariDataSource {

    private final Environment env;

    public MySqlConfig(Environment env) {
        this.env = env;
    }

    @Bean("mySqlEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(new HikariDataSource(this));
        em.setPackagesToScan("com.experiment.entity");
        em.setPersistenceUnitName("ExperimentMySql");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(additionProperties());
        em.getJpaPropertyMap().forEach((k,v) -> log.info("{}:{}", k, v));
        return em;

    }

    @Bean("mySqlTransactionManager")
    public PlatformTransactionManager platformTransactionManager(@Qualifier("mySqlEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setJpaProperties(additionProperties());
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    private Properties additionProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("mysql.hibernate.hbm2ddl.auto"));
        properties.setProperty("hibernate.dialect", env.getProperty("mysql.hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getProperty("mysql.hibernate.show_sql"));
        properties.put("hibernate.format_sql", env.getProperty("mysql.hibernate.format_sql"));
        properties.put("hibernate.use_sql_comments", env.getProperty("mysql.hibernate.use_sql_comments"));
        properties.put("hibernate.connection.CharSet", env.getProperty("mysql.hibernate.CharSet"));
        properties.put("hibernate.generate_statistics", env.getProperty("mysql.hibernate.generate_statistics"));
        properties.put("hibernate.connection.autocommit", env.getProperty("mysql.hibernate.autocommit"));
        properties.put("hibernate.connection.useUnicode", env.getProperty("mysql.hibernate.useUnicode"));
        properties.put("hibernate.order_inserts", env.getProperty("mysql.hibernate.order_inserts"));
        properties.put("hibernate.order_updates", env.getProperty("mysql.hibernate.order_inserts"));
        properties.put("hibernate.jdbc.batch_size", env.getProperty("mysql.hibernate.jdbc.batch_size"));
        properties.put("hibernate.enable_lazy_load_no_trans", env.getProperty("mysql.hibernate.enable_lazy_load_no_trans"));
        return properties;
    }

}
