package com.experiment.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

@Configuration
public class RedisConfig {

    @Bean("redissonClient")
    public RedissonClient redissonClient(@Value("${redis.config}") String configFile) throws IOException {
        File file = new File(configFile);
        Config config = Config.fromYAML(file);
        return Redisson.create(config);
    }
}
