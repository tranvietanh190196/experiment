package com.experiment.junit;


import com.experiment.ExperimentApplication;
import com.experiment.entity.Employee;
import com.experiment.job.HzPipeLine;
import com.experiment.util.Utils;
import com.hazelcast.collection.IList;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.pipeline.Pipeline;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExperimentApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.yml")
public class HazelcastJetEmbedded {

    @Autowired
    @Qualifier("jetEmbedded")
    private JetInstance jetEmbedded;
    private List<Employee> employees;


    @Before
    public void before() {
        employees = new ArrayList<>();
        for (int i = 0; i < 100_000; i++) {
            Employee e = new Employee(UUID.randomUUID().toString(), "VA", BigDecimal.valueOf((1000D * i)), "VND", "HN", 25);
            employees.add(e);
        }
    }

    @Test
    @Ignore
    public void writerTest(){
        IList<Employee> iList = jetEmbedded.getList("employees");
        long start = System.currentTimeMillis();
        int size = iList.size();
        iList.removeAll(iList);
        log.warn("Performance for delete {} record: {}ms", size, System.currentTimeMillis() - start);
        start = System.currentTimeMillis();
        Utils.batching(employees, 50_000, iList::addAll);
        log.warn("Performance for push {} record: {}ms", iList.size(), System.currentTimeMillis() - start);
    }

    @Test
    @Ignore
    public void readerTest(){
        long start = System.currentTimeMillis();
        IList<Employee> iList2 = jetEmbedded.getList("employees");
        List<Employee> temp = new ArrayList<>(iList2);
        log.warn("Performance for get {} record: {}ms", temp.size(), System.currentTimeMillis() - start);
    }

    @Test
    @Ignore
    public void jobTest() {
        writerTest();
        Pipeline pipeline = HzPipeLine.createPipeLine(jetEmbedded.getList("employees"));
        jetEmbedded.newJob(pipeline).join();
    }
}
