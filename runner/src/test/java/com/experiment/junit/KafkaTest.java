package com.experiment.junit;


import com.experiment.util.KafkaBuilder;
import com.experiment.util.Utils;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

@Log4j2
@RunWith(SpringRunner.class)
public class KafkaTest {

    private Producer<Long, String> kafkaProducer;
    private Consumer<Long, String> kafkaConsumer;
    private static final String TOPIC = "experiment";
    private ExecutorService executors;

    @Before
    public void setup(){
        kafkaProducer = KafkaBuilder.builder()
                .brokers("10.26.53.17:9092")
                .build().getProducer();
        kafkaConsumer = KafkaBuilder.builder()
                .brokers("10.26.53.17:9092")
                .autocommit(false)
                .maxPollRecords(100)
                .offsetReset(true)
                .messageCount(1000)
                .maxNoMessageFoundCount(100)
                .topic(TOPIC)
                .groupId(UUID.randomUUID().toString())
                .build().getConsumer();
        executors =  Executors.newFixedThreadPool(2);
    }

    @Test
    public void kafkaConsumer(){
        Runnable consumerKafka = () -> {
            while (true){
                ConsumerRecords<Long, String> records = kafkaConsumer.poll(Duration.ofSeconds(10));
                if(records.isEmpty()) continue;
                records.forEach(record -> {
                    log.info("Receive: [key:{}, value:{}, header:{}, offset:{}] - Thread:{}", record.key(), record.value(), record.headers(),record.offset(), Thread.currentThread().getId());
                });
                kafkaConsumer.commitAsync();
            }
        };
        executors.submit(consumerKafka);
        Utils.sleep(50000000);
    }

    @Test
    public void kafkaProducer(){
        new Thread(() -> {
            IntStream.rangeClosed(0, 1000).forEach(i -> {
                String mess = "This is record " + i;
                ProducerRecord<Long, String> record = new ProducerRecord<>(TOPIC, mess);
                kafkaProducer.send(record);
                Utils.sleep(1000);
            });
            log.info("Producer 1k records DONE!");
        }).start();
    }




}
