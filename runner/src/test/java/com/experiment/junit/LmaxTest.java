package com.experiment.junit;

import com.experiment.config.LmaxConfig;
import com.experiment.lmax.Event;
import com.experiment.util.Utils;
import com.lmax.disruptor.RingBuffer;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.IntStream;

@Log4j2
@RunWith(SpringRunner.class)
//@AutoConfigureMockMvc
@ContextConfiguration(classes = LmaxConfig.class)
public class LmaxTest {

    @Autowired
    @Qualifier("ring_buffer_2")
    private RingBuffer<Event> ringBuffer2;

    @Autowired
    @Qualifier("ring_buffer")
    private RingBuffer<Event> ringBuffer;

    @Test
    public void test(){
        IntStream.rangeClosed(0, 1 << 16).forEach(i -> {
            long sq = ringBuffer.next();
            Event event = ringBuffer.get(sq);
            event.setMessage(String.valueOf(i));
            ringBuffer.publish(sq);
        });
        Utils.sleep(50000);
    }

    @Test
    public void testSequenceBarrier(){
        IntStream.rangeClosed(0, 1 << 18).forEach(i -> {

            long sq = ringBuffer2.next();
            Event event = ringBuffer2.get(sq);
            event.setMessage(String.valueOf(i));
            ringBuffer2.publish(sq);
        });
    }
}
