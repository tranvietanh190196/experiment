package com.experiment.junit;

import com.experiment.entity.Employee;
import lombok.extern.log4j.Log4j2;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.UUID;


@Log4j2
@RunWith(SpringRunner.class)
//@SpringBootTest(classes = ExperimentApplication.class)
//@AutoConfigureMockMvc
@DataJpaTest
@TestPropertySource(locations = "classpath:application.yml")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.AUTO_CONFIGURED)
public class MySqlTransaction {

    @Autowired
    @Qualifier("mySqlEntityManagerFactory")
    private EntityManager entityManager;

    @Test
    @Ignore("Test is ignored as a demonstration")
    public void testBatch(){
        for(int i=0; i < 50000; i++){
            if(i > 0 && i % 1000 == 0){
                entityManager.flush();
                entityManager.clear();
                log.info("Committed batch {} record done!", 1000);
            }
            Employee employee = new Employee();
            employee.setId(UUID.randomUUID().toString());
            employee.setAddress("Hai Phong");
            employee.setName("Viet Anh Tran");
            employee.setAge(25);
            employee.setDescription("xxxxx");
            employee.setSalary(BigDecimal.valueOf(15_000_000));
            entityManager.persist(employee);
        }
    }
}
