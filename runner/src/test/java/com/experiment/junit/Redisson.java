package com.experiment.junit;

import com.experiment.ExperimentApplication;
import com.experiment.entity.Employee;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExperimentApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.yml")
public class Redisson {

    @Autowired
    private RedissonClient redissonClient;

    @Test
    @SneakyThrows
    @Ignore("Test is ignored as a demonstration")
    public void atomicLong() {
        log.info("REDIS ATOMICLONG TESTING...");
        RAtomicLong atomicLong = redissonClient.getAtomicLong("AtomicLong");
        log.info("GetAndDe:{}", atomicLong.getAndDecrement());
        log.info("GetAndIn:{}", atomicLong.getAndIncrement());
        log.info("AddAndGet(10L): {}", atomicLong.addAndGet(10L));
        log.info("CompareAndSet(29,412):{}", atomicLong.compareAndSet(29, 412));
        log.info("DeAndGet:{}", atomicLong.decrementAndGet());
        log.info("InAndGet:{}", atomicLong.incrementAndGet());
        log.info("GetAndAdd(302):{}", atomicLong.getAndAdd(302));
        log.info("GetAndDe:{}", atomicLong.getAndDecrement());
        log.info("GetAndIn:{}", atomicLong.getAndIncrement());

        boolean delete = atomicLong.deleteAsync().await(5, TimeUnit.SECONDS);
        Assert.assertTrue(delete);
    }

    @Test
    @Ignore("Test is ignored as a demonstration")
    public void objectLive() {
        log.info("REDIS LIVEOBJECT TESTING...");
        RLiveObjectService service = redissonClient.getLiveObjectService();
        Employee employee = new Employee();
        employee.setId(UUID.randomUUID().toString());
        employee.setName("TVA");
        employee.setDescription("xxxx");
        employee.setAge(25);

        service.persist(employee);
        service.findIds(Employee.class).forEach(k -> log.info("RLive key: {}", k));

        Employee get = service.get(Employee.class, "TVA");
        Assert.assertEquals(25, get.getAge());
    }

    @Test
    @Ignore("Test is ignored as a demonstration")
    public void multipleMap() {
        log.info("REDIS MULTIMAP TESTING...");
        RMultimap<String, String> multimap = redissonClient.getSetMultimap("MultiMap");
        multimap.readAllKeySet();

        multimap.put("k1", "v11");
        multimap.put("k1", "v12");
        multimap.put("k2", "v21");
        multimap.put("k2", "v22");

        Set<String> set = (Set<String>) multimap.get("k1");
        log.info("Value of Set: {}", set);
        multimap.remove("k1", "v11");
        set = (Set<String>) multimap.get("k1");
        log.info("Value of Set after remove k11_v11: {}", set);
    }

    @Test
    @SneakyThrows
    @Ignore("Test is ignored as a demonstration")
    public void sortedSet() {
        log.info("REDIS SORTEDSET TESTING...");

        RSortedSet<String> sortedSet = redissonClient.getSortedSet("SortedSet");
        sortedSet.addAsync("B");
        sortedSet.addAsync("C");
        sortedSet.addAsync("A");
        sortedSet.addAsync("D");
        sortedSet.addAsync("G");
        sortedSet.addAsync("E");

        sortedSet.readAllAsync().await().get().forEach(v -> log.info("{}", v));

        boolean delete = sortedSet.deleteAsync().await(5, TimeUnit.SECONDS);
        Assert.assertTrue(delete);
    }
}
