package com.experiment.batch;

public interface BatchProcessor {
    <E> void write(E e);
}
