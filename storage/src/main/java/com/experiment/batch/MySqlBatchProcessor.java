package com.experiment.batch;


import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Log4j2
@Profile("runner")
@Component("mySqlBatchProcessor")
@Transactional("mySqlTransactionManager")
public class MySqlBatchProcessor implements BatchProcessor {

    private int batchSize = 500;

    private final EntityManager entityManager;

    public MySqlBatchProcessor(@Qualifier("mySqlEntityManagerFactory") EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public <E> void write(E e) {
        entityManager.persist(e);
    }

    private void commit(){
        entityManager.flush();
        entityManager.clear();
    }

}
