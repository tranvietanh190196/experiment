package com.experiment.entity;

import com.experiment.util.Utils;
import lombok.*;
import org.redisson.api.annotation.REntity;
import org.redisson.api.annotation.RId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@REntity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employee")
@ToString
public class Employee implements Serializable {

    @Id
    private String id;
    @RId
    private String name;
    private BigDecimal salary;
    private String description;
    private String address;
    private int age;

    @Override
    public String toString() {
        return Utils.toString(this);
    }
}
