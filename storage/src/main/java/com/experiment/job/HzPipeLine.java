package com.experiment.job;

import com.experiment.entity.Employee;
import com.hazelcast.collection.IList;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.Sources;
import com.hazelcast.map.IMap;

import java.math.BigDecimal;

public class HzPipeLine {

    public static Pipeline createPipeLine(IList<Employee> iList) {
        Pipeline p = Pipeline.create();
        p.readFrom(Sources.list(iList))
                .setLocalParallelism(1)
                .filter(employee -> employee.getSalary().compareTo(BigDecimal.valueOf(50_000D)) > 0)
                .writeTo(Sinks.files("out"));
        return p;
    }

    public static Pipeline createPipeLine(IMap<Integer, String> iMap) {
        Pipeline p = Pipeline.create();
        p.readFrom(Sources.map(iMap))
                .filter(e -> e.getKey() % 2 == 0)
                .writeTo(Sinks.logger());
        return p;
    }
}
