package com.experiment.lmax;

import com.lmax.disruptor.EventFactory;

public  class BaseEventFactory implements EventFactory<Event> {

    @Override
    public  Event newInstance() {
        return new Event();
    }
}
