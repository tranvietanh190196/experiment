package com.experiment.lmax;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Event {
    private String message;
}
