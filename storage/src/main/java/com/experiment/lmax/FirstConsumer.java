package com.experiment.lmax;


import com.experiment.util.Utils;
import com.lmax.disruptor.EventHandler;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class FirstConsumer implements EventHandler<Event> {

    @Override
    @SneakyThrows
    public void onEvent(Event event, long sequence, boolean b) {
        if(sequence % 2 == 0){
            log.info("C1_THREAD:{} -> {}", Thread.currentThread().getId(), event.getMessage());
            Utils.sleep(10);
        }
    }
}
