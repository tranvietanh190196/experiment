package com.experiment.lmax;

import com.experiment.util.Utils;
import com.lmax.disruptor.EventHandler;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class SecondConsumer implements EventHandler<Event> {
    @Override
    public void onEvent(Event event, long sq, boolean b) throws Exception {
        if(sq % 2 != 0) {
            log.info("C2_THREAD:{} -> {}", Thread.currentThread().getId(), event.getMessage());
            Utils.sleep(10);
        }
    }
}
