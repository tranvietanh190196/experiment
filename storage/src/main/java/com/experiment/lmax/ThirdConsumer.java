package com.experiment.lmax;

import com.lmax.disruptor.EventHandler;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class ThirdConsumer implements EventHandler<Event> {

    @Override
    public void onEvent(Event event, long l, boolean b) throws Exception {
//        Utils.sleep(500);
    }
}
