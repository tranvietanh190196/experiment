package com.experiment.lmax;

import com.lmax.disruptor.EventTranslatorOneArg;

public class Translator {

    public static EventTranslatorOneArg<Event, String> TRANSLATOR = (event, squeue, mess) -> {
        event.setMessage(mess);
    };
}
