package com.experiment.serialization;

import com.experiment.entity.Employee;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.Serializer;
import com.hazelcast.nio.serialization.StreamSerializer;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;

@Component
public class EmployeeSerializer implements ISerializer, StreamSerializer<Employee> {

    @Override
    public void write(ObjectDataOutput out, Employee employee) throws IOException {
        out.writeUTF(employee.getId());
        out.writeUTF(employee.getName());
        out.writeUTF(employee.getAddress());
        out.writeUTF(employee.getDescription());
        out.writeInt(employee.getAge());
        out.writeDouble(employee.getSalary().doubleValue());
    }

    @Override
    public Employee read(ObjectDataInput in) throws IOException {
        Employee employee = new Employee();
        employee.setId(in.readUTF());
        employee.setName(in.readUTF());
        employee.setAddress(in.readUTF());
        employee.setDescription(in.readUTF());
        employee.setAge(in.readInt());
        employee.setSalary(BigDecimal.valueOf(in.readDouble()));
        return employee;
    }

    @Override
    public int getTypeId() {
        return 1;
    }

    @Override
    public Class getClassParameter() {
        return Employee.class;
    }

    @Override
    public Serializer getSerializer() {
        return this;
    }
}
