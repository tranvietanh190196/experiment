package com.experiment.serialization;

import com.hazelcast.config.SerializerConfig;
import com.hazelcast.nio.serialization.Serializer;

public interface ISerializer {
      <T> Class<T> getClassParameter();
      Serializer getSerializer();

    default SerializerConfig toSerializerConfig(){
        return new SerializerConfig().setImplementation(getSerializer()).setTypeClass(getClassParameter());
    }
}
