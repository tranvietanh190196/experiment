package com.experiment.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.FatalExceptionHandler;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.lmax.disruptor.util.DaemonThreadFactory;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Log4j2
public class Utils {

    private Utils() {
    }

    private static Gson gson;
    private static ObjectMapper objectMapper;

    static {
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .serializeSpecialFloatingPointValues()
                .create();
        objectMapper = new ObjectMapper();
    }


    public static String toString(Object object) {
        try {
            return gson.toJson(object);
        } catch (Exception e) {
            log.error("Failed to string json!", e);
            return "";
        }
    }

    public static <T> Disruptor<T> createSingleProducer(EventFactory<T> eventFactory, WaitStrategy waitStrategy, int ringSize) {
        Disruptor<T> disruptor = new Disruptor(
                eventFactory,
                ringSize,
                DaemonThreadFactory.INSTANCE,
                ProducerType.SINGLE,
                waitStrategy);
        disruptor.setDefaultExceptionHandler(new FatalExceptionHandler());
        return disruptor;
    }

    public static  <T> void batching(List<T> in, int size, Consumer<List<T>> consumer){
        for(int i = 0; i < in.size()/size + 1; i++){
            List<T> temp = in.parallelStream()
                    .skip((long) i * size)
                    .limit(size)
                    .collect(Collectors.toList());
            consumer.accept(temp);
        }
    }

    @SneakyThrows
    public static void sleep(long time) {
        Thread.sleep(time);
    }
}
